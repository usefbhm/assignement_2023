package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.domain.Utilisateur;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


@DataJpaTest
class DepositRepositoryTest {
    @Autowired
    DepositRepository repository;


    @Test
    public void findOne(){
        MoneyDeposit md1 = new MoneyDeposit(
                new BigDecimal(5),
                new Date(),
                "youssef bahomman",
                new Compte(),
                "testMotif"
                );
        repository.save(md1);

        MoneyDeposit md2 = new MoneyDeposit(
                new BigDecimal(5),
                new Date(),
                "ahmed bahomman",
                new Compte(),
                "testMotif2"
        );
        repository.save(md2);

        MoneyDeposit foundDeposit = repository.findById(md2.getId()).get();

        assertThat(foundDeposit).isEqualTo(md2);
    }
    @Test
    public void findAll() {
        List<MoneyDeposit> moneyDeposits = repository.findAll();

        assertThat(moneyDeposits).isEmpty();
    }

    @Test
    public void save() {
        MoneyDeposit moneyDeposit = repository.save(
                new MoneyDeposit(
                        new BigDecimal(5),
                        new Date(),
                        "youssef bahomman",
                        new Compte(),
                        "testMotif"
                ));
        assertThat(moneyDeposit).hasFieldOrProperty("id");
        assertThat(moneyDeposit).hasFieldOrProperty("dateExecution");
        assertThat(moneyDeposit).hasFieldOrProperty("compteBeneficiaire");
        assertThat(moneyDeposit).hasFieldOrPropertyWithValue("montant", BigDecimal.valueOf(5));
        assertThat(moneyDeposit).hasFieldOrPropertyWithValue("nom_prenom_emetteur", "youssef bahomman");
        assertThat(moneyDeposit).hasFieldOrPropertyWithValue("motifDeposit", "testMotif");
    }

    @Test
    public void delete() {
        MoneyDeposit md1 = new MoneyDeposit(
                new BigDecimal(5),
                new Date(),
                "youssef bahomman",
                new Compte(),
                "testMotif"
        );
        repository.save(md1);

        MoneyDeposit md2 = new MoneyDeposit(
                new BigDecimal(5),
                new Date(),
                "ahmed bahomman",
                new Compte(),
                "testMotif2"
        );
        repository.save(md2);

        repository.deleteById(md1.getId());

        Optional<MoneyDeposit> moneyDeposits = repository.findById(md1.getId());

        assertThat(moneyDeposits).isEmpty();
    }

}