package ma.octo.assignement.web;

import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.service.DepositService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DepositControllerTest {
    @Autowired
    DepositService depositService;

    @Test
    void shouldThrowCompteNonExistantExceptionWhenRipIsEmpty(){
        DepositDto depositDto = new DepositDto();
        Exception exception = assertThrows(DepositException.class, ()->{
            depositService.createDeposit(depositDto);
        });
        assertEquals("Rip vide", exception.getMessage());
    }
    @Test
    void shouldThrowCompteNonExistantExceptionWhenRipFormatNotValid(){
        DepositDto depositDto = new DepositDto();
        depositDto.setRib("2145123");
        Exception exception = assertThrows(DepositException.class, ()->{
            depositService.createDeposit(depositDto);
        });
        assertEquals("Rip pas valide", exception.getMessage());
    }
    @Test
    void shouldThrowCompteNonExistantExceptionWhenNoAccountIsNotFound(){
        DepositDto depositDto = new DepositDto();
        String randomRip = "245613245789453214785463";
        depositDto.setRib(randomRip);
        Exception exception = assertThrows(CompteNonExistantException.class, ()->{
            depositService.createDeposit(depositDto);
        });
        assertEquals("Compte Non existant", exception.getMessage());
    }
    @Test
    void shouldThrewDepositExceptionWithMessageWhenSoldIsNull(){
        DepositDto depositDto = new DepositDto();
        String user1Rib = "240165784125784156985124";
        depositDto.setRib(user1Rib);
        Exception exception = assertThrows(DepositException.class, ()->{
            depositService.createDeposit(depositDto);
        });
        assertEquals("Montant vide", exception.getMessage());
    }

    @Test
    void shouldThrewDepositExceptionWithMessageWhenSoldIsZero(){
        DepositDto depositDto = new DepositDto();
        String user1Rib = "240165784125784156985124";
        depositDto.setRib(user1Rib);
        depositDto.setMontant(BigDecimal.valueOf(0));
        Exception exception = assertThrows(DepositException.class, ()->{
            depositService.createDeposit(depositDto);
        });
        assertEquals("Montant vide", exception.getMessage());
    }
    @Test
    void shouldThrewDepositExceptionWithMessageWhenSoldIsLessThan10(){
        DepositDto depositDto = new DepositDto();
        String user1Rib = "240165784125784156985124";
        depositDto.setRib(user1Rib);
        depositDto.setMontant(BigDecimal.valueOf(5));
        Exception exception = assertThrows(DepositException.class, ()->{
            depositService.createDeposit(depositDto);
        });
        assertEquals("Montant minimal de deposit non atteint", exception.getMessage());
    }
    @Test
    void shouldThrewDepositExceptionWithMessageWhenSoldIsMoreThan1000(){
        DepositDto depositDto = new DepositDto();
        String user1Rib = "240165784125784156985124";
        depositDto.setRib(user1Rib);
        depositDto.setMontant(BigDecimal.valueOf(5000));
        Exception exception = assertThrows(DepositException.class, ()->{
            depositService.createDeposit(depositDto);
        });
        assertEquals("Montant maximal de deposit dépassé", exception.getMessage());
    }
    @Test
    void shouldThrewDepositExceptionWithMessageWhenEmmeteurNullOrEmpty(){
        DepositDto depositDto = new DepositDto();
        String user1Rib = "240165784125784156985124";
        depositDto.setRib(user1Rib);
        depositDto.setMontant(BigDecimal.valueOf(100));
        Exception exception = assertThrows(DepositException.class, ()->{
            depositService.createDeposit(depositDto);
        });
        assertEquals("Emetteur Nom et Prenom vide", exception.getMessage());
    }

    @Test
    void shouldThrewDepositExceptionWithMessageWhenMotifNullOrEmpty(){
        DepositDto depositDto = new DepositDto();
        String user1Rib = "240165784125784156985124";
        depositDto.setRib(user1Rib);
        depositDto.setMontant(BigDecimal.valueOf(100));
        depositDto.setNom_prenom_emetteur("emeteur test");
        Exception exception = assertThrows(DepositException.class, ()->{
            depositService.createDeposit(depositDto);
        });
        assertEquals("Motif vide", exception.getMessage());
    }
}