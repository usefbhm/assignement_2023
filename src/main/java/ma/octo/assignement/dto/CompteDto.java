package ma.octo.assignement.dto;

import ma.octo.assignement.domain.Utilisateur;

import javax.persistence.*;
import java.math.BigDecimal;

public class CompteDto {
    private String nrCompte;
    private String rib;
    private BigDecimal solde;

    public CompteDto() {
    }

    public CompteDto(String nrCompte, String rib, BigDecimal solde) {
        this.nrCompte = nrCompte;
        this.rib = rib;
        this.solde = solde;
    }

    public String getNrCompte() {
        return nrCompte;
    }

    public void setNrCompte(String nrCompte) {
        this.nrCompte = nrCompte;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public BigDecimal getSolde() {
        return solde;
    }

    public void setSolde(BigDecimal solde) {
        this.solde = solde;
    }
}
