package ma.octo.assignement.dto;

import ma.octo.assignement.domain.Compte;

import java.math.BigDecimal;
import java.util.Date;

public class DepositDto {

    private BigDecimal Montant;
    private Date dateExecution;
    private String nom_prenom_emetteur;
    private String Rib;
    private String motifDeposit;

    public BigDecimal getMontant() {
        return Montant;
    }

    public DepositDto() {
    }

    public DepositDto(BigDecimal montant, Date dateExecution, String nom_prenom_emetteur, String rib, String motifDeposit) {
        Montant = montant;
        this.dateExecution = dateExecution;
        this.nom_prenom_emetteur = nom_prenom_emetteur;
        Rib = rib;
        this.motifDeposit = motifDeposit;
    }

    public void setMontant(BigDecimal montant) {
        this.Montant = montant;
    }

    public Date getDateExecution() {
        return dateExecution;
    }

    public void setDateExecution(Date dateExecution) {
        this.dateExecution = dateExecution;
    }

    public String getNom_prenom_emetteur() {
        return nom_prenom_emetteur;
    }

    public void setNom_prenom_emetteur(String nom_prenom_emetteur) {
        this.nom_prenom_emetteur = nom_prenom_emetteur;
    }

    public String getRib() {
        return Rib;
    }

    public void setRib(String rib) {
        Rib = rib;
    }

    public String getMotifDeposit() {
        return motifDeposit;
    }

    public void setMotifDeposit(String motifDeposit) {
        this.motifDeposit = motifDeposit;
    }

    @Override
    public String toString() {
        return "DepositDto{" +
                "montant=" + Montant +
                ", dateExecution=" + dateExecution +
                ", nom_prenom_emetteur='" + nom_prenom_emetteur + '\'' +
                ", Rib='" + Rib + '\'' +
                ", motifDeposit='" + motifDeposit + '\'' +
                '}';
    }
}
