package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.web.TransferController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TransferService {
    public static final int MONTANT_MAXIMAL = 10000;

    Logger LOGGER = LoggerFactory.getLogger(TransferService.class);

    @Autowired
    private CompteRepository rep1;
    @Autowired
    private TransferRepository re2;
    @Autowired
    private AuditService monservice;
    private final UtilisateurRepository re3;

    @Autowired
    public TransferService(UtilisateurRepository re3) {
        this.re3 = re3;
    }

    public List<TransferDto> loadAll() {
        LOGGER.info("Lister des utilisateurs");
        var all = re2.findAll();
        List<TransferDto> transferDtos = new ArrayList<>();
        for (Transfer transfer:all){
            transferDtos.add(new TransferDto(transfer.getCompteEmetteur().getNrCompte(), transfer.getCompteBeneficiaire().getNrCompte(), transfer.getMotifTransfer(), transfer.getMontantTransfer(), transfer.getDateExecution()));
        }
        return CollectionUtils.isEmpty(all) ? null : transferDtos;
    }

    public TransferDto createTransaction(TransferDto transferDto)
                    throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException{
        Optional<Compte> compteOptional1 = rep1.findByNrCompte(transferDto.getNrCompteEmetteur());
        Optional<Compte> compteOptional2 = rep1.findByNrCompte(transferDto.getNrCompteBeneficiaire());
        if (compteOptional1.isEmpty()) {
            LOGGER.error("Compte Emetteur Non existant");
            throw new CompteNonExistantException("Compte Emetteur Non existant");
        }

        if (compteOptional2.isEmpty()) {
            LOGGER.error("Compte Beneficiaire Non existant");
            throw new CompteNonExistantException("Compte Beneficiaire Non existant");
        }
        Compte c1 = compteOptional1.get();
        Compte f12 = compteOptional2.get();

        if (transferDto.getMontant() == null) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (transferDto.getMontant().intValue() == 0) {
            LOGGER.error("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (transferDto.getMontant().intValue() < 10) {
            LOGGER.error("Montant minimal de transfer non atteint");
            throw new TransactionException("Montant minimal de transfer non atteint");
        } else if (transferDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de transfer dépassé");
            throw new TransactionException("Montant maximal de transfer dépassé");
        }

        if (transferDto.getMotif().length() < 0) {
            LOGGER.error("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (c1.getSolde().intValue() - transferDto.getMontant().intValue() < 0) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new TransactionException("Solde insuffisant pour l'utilisateur");
        }

        c1.setSolde(c1.getSolde().subtract(transferDto.getMontant()));
        rep1.save(c1);

        f12.setSolde(new BigDecimal(f12.getSolde().intValue() + transferDto.getMontant().intValue()));
        rep1.save(f12);

        Transfer transfer = new Transfer();
        transfer.setDateExecution(new Date());
        transfer.setCompteBeneficiaire(f12);
        transfer.setCompteEmetteur(c1);
        transfer.setMotifTransfer(transferDto.getMotif());
        transfer.setMontantTransfer(transferDto.getMontant());
        re2.save(transfer);

        TransferDto dto = new TransferDto();
        dto.setDate(transfer.getDateExecution());
        dto.setMotif(transfer.getMotifTransfer());
        dto.setMontant(transfer.getMontantTransfer());
        dto.setNrCompteEmetteur(transfer.getCompteEmetteur().getNrCompte());
        dto.setNrCompteBeneficiaire(transfer.getCompteBeneficiaire().getNrCompte());

        monservice.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers " + transferDto
                .getNrCompteBeneficiaire() + " d'un montant de " + transferDto.getMontant()
                .toString());
        return dto;
    }
}
