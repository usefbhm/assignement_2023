package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class AccountService {

    @Autowired
    private CompteRepository rep1;

    private final UtilisateurRepository re3;

    @Autowired
    public AccountService(UtilisateurRepository re3) {
        this.re3 = re3;
    }

    Logger LOGGER = LoggerFactory.getLogger(AccountService.class);
    public List<CompteDto> loadAllCompte() {
        LOGGER.info("Liste Les Comptes");
        List<Compte> all = rep1.findAll();
        List<CompteDto> compteDtos = new ArrayList<>();
        for (Compte compte : all) {
            compteDtos.add(new CompteDto(compte.getNrCompte(), compte.getRib(), compte.getSolde()));
        }
        return CollectionUtils.isEmpty(all) ? null : compteDtos;
    }

    public List<UtilisateurDto> loadAllUtilisateur() {
        LOGGER.info("lister les Utilisateur");
        List<Utilisateur> all = re3.findAll();
        List<UtilisateurDto> utilisateurDtos = new ArrayList<>();
        for (Utilisateur utilisateur : all) {
            utilisateurDtos.add(new UtilisateurDto(utilisateur.getUsername(),
                                        utilisateur.getGender(),
                                        utilisateur.getLastname(),
                                        utilisateur.getFirstname(),
                                        utilisateur.getBirthdate()));
        }
        return CollectionUtils.isEmpty(all) ? null : utilisateurDtos;
    }
}
