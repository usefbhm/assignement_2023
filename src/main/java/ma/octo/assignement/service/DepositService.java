package ma.octo.assignement.service;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;
import ma.octo.assignement.web.DepositController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestBody;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class DepositService {

    public static final int MONTANT_MAXIMAL = 1000;
    @Autowired
    private DepositRepository depositRepository;
    @Autowired
    private CompteRepository compteRepository;
    @Autowired
    private AuditService monservice;
    Logger LOGGER = LoggerFactory.getLogger(DepositService.class);

    public List<DepositDto> loadAll(){
        LOGGER.info("lister les deposits");
        List<MoneyDeposit> all = depositRepository.findAll();
        List<DepositDto> depositDtos = new ArrayList<>();
        for(MoneyDeposit moneyDeposit: all){
            depositDtos.add(new DepositDto(moneyDeposit.getMontant(), moneyDeposit.getDateExecution(), moneyDeposit.getNom_prenom_emetteur(), moneyDeposit.getCompteBeneficiaire().getRib(), moneyDeposit.getMotifDeposit()));
        }
        return CollectionUtils.isEmpty(all) ?  null:  depositDtos;
    }

    public DepositDto createDeposit(DepositDto depositDto)
            throws CompteNonExistantException, DepositException{

        if(depositDto.getRib() == null){
            LOGGER.error("Rip vide");
            throw new DepositException("Rip vide");
        } else if (depositDto.getRib().length() != 24){
            LOGGER.error("Rip pas valide");
            throw new DepositException("Rip pas valide");
        }
        Optional<Compte> compteOptional = compteRepository.findByRib(depositDto.getRib());
        if (compteOptional.isEmpty()) {
            LOGGER.error("Compte Non existant");
            throw new CompteNonExistantException("Compte Non existant");
        }
        Compte c = compteOptional.get();
        if (depositDto.getMontant() == null) {
            LOGGER.error("Montant vide");
            throw new DepositException("Montant vide");
        }else if (depositDto.getMontant().intValue() == 0) {
            LOGGER.error("Montant vide");
            throw new DepositException("Montant vide");
        } else if (depositDto.getMontant().intValue() < 10) {
            LOGGER.error("Montant minimal de deposit non atteint");
            throw new DepositException("Montant minimal de deposit non atteint");
        } else if (depositDto.getMontant().intValue() > MONTANT_MAXIMAL) {
            LOGGER.error("Montant maximal de deposit dépassé");
            throw new DepositException("Montant maximal de deposit dépassé");
        }

        if( depositDto.getNom_prenom_emetteur() == null || depositDto.getNom_prenom_emetteur().length() < 0 ){
            LOGGER.error("Emetteur Nom et Prenom vide");
            throw new DepositException(("Emetteur Nom et Prenom vide"));
        }
        if (depositDto.getMotifDeposit()==null || depositDto.getMotifDeposit().length() < 0) {
            LOGGER.error("Motif vide");
            throw new DepositException("Motif vide");
        }

        c.setSolde(new BigDecimal(c.getSolde().intValue() +  depositDto.getMontant().intValue()));
        compteRepository.save(c);

        MoneyDeposit deposit = new MoneyDeposit();
        deposit.setMontant(depositDto.getMontant());
        deposit.setCompteBeneficiaire(c);
        deposit.setMotifDeposit(depositDto.getMotifDeposit());
        deposit.setDateExecution(new Date());
        deposit.setNom_prenom_emetteur(depositDto.getNom_prenom_emetteur());

        depositRepository.save(deposit);

        DepositDto newDepositDto = new DepositDto();
        newDepositDto.setRib(deposit.getCompteBeneficiaire().getRib());
        newDepositDto.setMotifDeposit(deposit.getMotifDeposit());
        newDepositDto.setMontant(deposit.getMontant());
        newDepositDto.setNom_prenom_emetteur(deposit.getNom_prenom_emetteur());
        newDepositDto.setDateExecution(deposit.getDateExecution());

        monservice.auditDeposit("Deposit vers le compt de RIP"+ depositDto.getRib()+" par  "+ depositDto.getNom_prenom_emetteur());

        return newDepositDto;
    }
}
