package ma.octo.assignement.web;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping("lister_utilisateurs")
    List<UtilisateurDto> loadAllUtilisateur() {
        return accountService.loadAllUtilisateur();
    }

    @GetMapping("listOfAccounts")
    List<CompteDto> loadAllCompte() {
        return accountService.loadAllCompte();
    }
}
