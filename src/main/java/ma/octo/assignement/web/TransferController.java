package ma.octo.assignement.web;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.TransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("transfers")
public class TransferController {
    @Autowired
    private TransferService transferService;

    @GetMapping("listDesTransferts")
    List<TransferDto> loadAll() {
        return transferService.loadAll();
    }
    @PostMapping("executerTransfers")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<TransferDto> createTransaction(@RequestBody TransferDto transferDto) {
        try {
            TransferDto newTransfer = transferService.createTransaction(transferDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(newTransfer)  ;
        } catch (SoldeDisponibleInsuffisantException | CompteNonExistantException | TransactionException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
