package ma.octo.assignement.web;

import ma.octo.assignement.domain.MoneyDeposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.DepositException;
import ma.octo.assignement.service.DepositService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("deposit")
public class DepositController {

    @Autowired
    private DepositService depositService;

    @GetMapping("listDesDeposits")
    public List<DepositDto> loadAll(){
        return depositService.loadAll();
    }

    @PostMapping("executerDeposit")
    public ResponseEntity<DepositDto> createDeposit(@RequestBody DepositDto depositDto){
        try{
            DepositDto newDepositDto = depositService.createDeposit(depositDto);
            return ResponseEntity.status(HttpStatus.CREATED).body(newDepositDto);
        }catch (CompteNonExistantException | DepositException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
